## Semantic Search Library

[[_TOC_]] 

## 📝 Overview

The SemanticSearch library for .NET provides a search engine implementation for RDF-based metadata graphs in Coscine, leveraging Virtuoso and ElasticSearch to enable fast and efficient searching and indexing of large amounts of metadata. By providing a mapping of the data and integrating with various underlying technologies, the SemanticSearch library simplifies the implementation of search within Coscine applications and workflows.

The code is created in the internal master thesis ["An Efficient Semantic Search Engine for Research Data in an RDF-based Knowledge Graph"](http://dx.doi.org/10.18154/RWTH-2020-09883) and further refined by us. Its documentation can be found under *`./docs/index.html`*.
## ⚙️ Configuration

To install this library, you can use the NuGet package manager or you can install it using the .NET CLI.
```powershell
Install-Package Coscine.SemanticSearch
```
or using the .NET CLI
```powershell
dotnet add package Coscine.SemanticSearch
```
### Prerequisites:

- Virtuoso
    - Virtuoso.ini file 
        - NumberOfBuffers >= 660000
        - MaxDirtyBuffers >= 495000
    - Define the following graphs as rulesets named "ruleset" for inference rules with `rdfs_rule_set ('<rulesetname>', '<graphname>') ;`
        - http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/
        - http://www.w3.org/ns/org#
        - http://xmlns.com/foaf/0.1/
- Data mentioned in ["Sample Dataset for Search Engine Evaluation for Research Data in an RDF-based Knowledge Graph"](http://dx.doi.org/10.18154/RWTH-2020-09886 )
    - All named_graphs, use virtuoso.db/graphs.db
- Elasticsearch
- Specified packages

## 📖 Usage

For examples on how we use this library, look into the source code of the following open-source [Coscine APIs](https://git.rwth-aachen.de/coscine/backend/apis) found in our public GitLab repository:
- `Search`
... and others.

### How to use:

1. (Re-)Build solution (Install/restore necessary packages)
2. Start Virtuoso
3. Start Elasticsearch
4. Run *Coscine.SemanticSearch.Cmd.exe* with the necessary arguments
5. Find your index name under `http://localhost:9200/_cat/indices/`
6. Preview your index at `http://localhost:9200/<INDEXNAME>/`
7. Preview data for index at `http://localhost:9200/<INDEXNAME>/_search`

### Commandline arguments

| Option        | Description | 
| ------------- |-------------| 
|-a, --action   | Required. Possible action: search, reindex, index, delete or add|
|-m             | (Default: http://localhost:8890/sparql) The link to the SPARQL connection|
|-e             | (Default: localhost) Server name of Elasticsearch|
|--ep           | (Default: 9200) Port of Elasticsearch|
|-d, --doc      | ID of metadata graph|
|-q, --query    | (Default: *) Elasticsearch query|
|--adv          | (Default: false) Set true for advanced Elasticsearch search syntax|
|-u, --user     | (Default: ) Specify user or only public metadata records could be found|

## 👥 Contributing

As an open source plattform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!