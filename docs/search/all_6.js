var searchData=
[
  ['foaf_90',['FOAF',['../class_semantic_search_implementation_1_1_uris.html#a7bfdd4e689fd1aeafc6a87614c3a5701',1,'SemanticSearchImplementation::Uris']]],
  ['foaf_5ffamily_5fname_91',['FOAF_FAMILY_NAME',['../class_semantic_search_implementation_1_1_uris.html#af2e66ba460643e77e5653477c6363fe6',1,'SemanticSearchImplementation::Uris']]],
  ['foaf_5fgiven_5fname_92',['FOAF_GIVEN_NAME',['../class_semantic_search_implementation_1_1_uris.html#ac7e6436aa09256312dc0013b35f8c1b5',1,'SemanticSearchImplementation::Uris']]],
  ['foaf_5fname_93',['FOAF_NAME',['../class_semantic_search_implementation_1_1_uris.html#adf43c704d8ed1d435970255ef7cd5a07',1,'SemanticSearchImplementation::Uris']]],
  ['foaf_5fperson_94',['FOAF_PERSON',['../class_semantic_search_implementation_1_1_uris.html#a3028ddcf4f9639cf02d030403fe5ede5',1,'SemanticSearchImplementation::Uris']]]
];
