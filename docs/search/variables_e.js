var searchData=
[
  ['rdf_417',['RDF',['../class_semantic_search_implementation_1_1_uris.html#a212997e1ebc87e8b0c0c5e4ee21055d2',1,'SemanticSearchImplementation::Uris']]],
  ['rdf_5ftype_5flong_418',['RDF_TYPE_LONG',['../class_semantic_search_implementation_1_1_uris.html#a35f8b9626785a8781fd105a200d00240',1,'SemanticSearchImplementation::Uris']]],
  ['rdfs_419',['RDFS',['../class_semantic_search_implementation_1_1_uris.html#af78d25ac43f5f2b9e43bd544f9099470',1,'SemanticSearchImplementation::Uris']]],
  ['rdfs_5flabel_420',['RDFS_LABEL',['../class_semantic_search_implementation_1_1_uris.html#a9554faa858bdbdfeeac34a042daff151',1,'SemanticSearchImplementation::Uris']]],
  ['rdfs_5fsubclass_5fof_421',['RDFS_SUBCLASS_OF',['../class_semantic_search_implementation_1_1_uris.html#ad19dd21c8b3935b0cf41595c34c9b031',1,'SemanticSearchImplementation::Uris']]],
  ['reason_422',['Reason',['../class_semantic_search_implementation_1_1_not_indexable_exception.html#a52ef9eeaf159ce899e1519eb3f3f482d',1,'SemanticSearchImplementation::NotIndexableException']]],
  ['reindex_423',['REINDEX',['../class_semantic_search_implementation_1_1_semantic_search.html#a3df2e52026edac177f0f2155af4b71c5',1,'SemanticSearchImplementation::SemanticSearch']]],
  ['ruleset_5fquery_424',['RULESET_QUERY',['../class_semantic_search_implementation_1_1_virtuoso_rdf_connector.html#a3fa556b03971c64b7aa44586a89bf14f',1,'SemanticSearchImplementation::VirtuosoRdfConnector']]]
];
