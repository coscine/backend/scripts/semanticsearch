﻿using Coscine.SemanticSearch.Clients;
using Coscine.SemanticSearch.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using VDS.RDF;
using VDS.RDF.Query;

namespace Coscine.SemanticSearch.Rules;

/// <summary>
/// Represents a literal rule.
/// </summary>
public class LiteralRule
{
    private readonly IRdfConnector _connector;
    private readonly string _queryString;
    private readonly INamespaceMapper _namespaceMapper;
    private readonly List<string> _languages;

    public LiteralRule(IRdfConnector connector, string queryString, INamespaceMapper namespaceMapper, List<string> languageList)
    {
        _connector = connector;
        _queryString = queryString;
        _namespaceMapper = namespaceMapper;
        _languages = languageList;
    }

    /// <summary>
    /// Executes a literal rule for a given instance of a class.
    /// </summary>
    /// <param name="instance">A URI of a concrete instance of a class.</param>
    /// <returns>A list of constructed literals.</returns>
    public IEnumerable<string> ExecuteRule(string instance)
    {
        var sparqlParameterizedString = new SparqlParameterizedString
        {
            CommandText = _queryString,
            Namespaces = _namespaceMapper
        };
        sparqlParameterizedString.SetUri(RdfClient.LABEL_LITERAL_RULE, new Uri(instance));
        var result = _connector.QueryWithResultGraph(sparqlParameterizedString);
        return result.Triples.Where(x =>
        {
            var language = ((ILiteralNode)x.Object).Language;
            return Equals(language, _languages) || string.Equals(language, string.Empty);
        }).Select(x => ((ILiteralNode)x.Object).Value).Distinct();
    }
}