﻿using AngleSharp.Common;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.SemanticSearch.Util;

public class Document
{
    public Dictionary<string, bool> bools;
    public Dictionary<string, IEnumerable<string>> collections;
    public Dictionary<string, Document> documents;
    public Dictionary<string, string> values;

    public Document()
    {
        bools = new Dictionary<string, bool>();
        collections = new Dictionary<string, IEnumerable<string>>();
        documents = new Dictionary<string, Document>();
        values = new Dictionary<string, string>();
    }

    public Document(string key, bool value) : this()
    {
        bools.Add(key, value);
    }

    public Document(string key, string value) : this()
    {
        values.Add(key, value);
    }

    public Document(string key, IEnumerable<string> value) : this()
    {
        collections.Add(key, value);
    }

    public Document(string key, Document value) : this()
    {
        documents.Add(key, value);
    }

    public void Add(string key, IEnumerable<string> value)
    {
        collections.TryAdd(key, value);
    }

    public void Add(string key, Document value)
    {
        documents.TryAdd(key, value);
    }

    public void Add(string key, string value)
    {
        values.TryAdd(key, value);
    }

    public void Add(string key, bool value)
    {
        bools.TryAdd(key, value);
    }

    public void Add(string key, int value)
    {
        values.TryAdd(key, value + "");
    }

    public void Merge(Document document)
    {
        // Merge bools
        foreach (var kv in document.bools)
        {
            bools.TryAdd(kv.Key, kv.Value);
        }

        // Merge collections and extend the collection, if the same key is used
        foreach (var kv in document.collections)
        {
            if (collections.TryGetValue(kv.Key, out var currentCollection))
            {
                collections[kv.Key] = currentCollection.Concat(kv.Value);
            }
            else
            {
                collections[kv.Key] = kv.Value;
            }
        }

        // Merge the documents
        foreach (var kv in document.documents)
        {
            documents.TryAdd(kv.Key, kv.Value);
        }

        // Merge the values
        // Should the key be already present, promote both values to collection
        // Should a collection for this key already exists,
        // add it to the collection and not to the values!
        foreach (var kv in document.values)
        {
            // The key is already in a collection, so add it to it
            if (collections.TryGetValue(kv.Key, out var currentCollection))
            {
                collections[kv.Key] = currentCollection.Concat(kv.Value);
            }
            // The Key is is not in a collection, but already in values
            // Remove old value from values and at bot to a collection
            else if (values.TryGetValue(kv.Key, out var currentValue))
            {
                values.Remove(kv.Key);
                collections[kv.Key] = new List<string> { currentValue, kv.Value };
            }
            // The key is unknown and can be added to values
            else
            {
                values.Add(kv.Key, kv.Value);
            }
        }
    }

    public override string ToString()
    {
        var finalDict = new Dictionary<string, object>();
        foreach (var kv in bools)
        {
            finalDict.TryAdd(kv.Key, kv.Value);
        }
        foreach (var kv in collections)
        {
            finalDict.TryAdd(kv.Key, kv.Value);
        }
        foreach (var kv in documents)
        {
            finalDict.TryAdd(kv.Key, JsonConvert.SerializeObject(kv.Value.ToString()));
        }
        foreach (var kv in values)
        {
            finalDict.TryAdd(kv.Key, kv.Value);
        }
        return JsonConvert.SerializeObject(finalDict);
    }
}