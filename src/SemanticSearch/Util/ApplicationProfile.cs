﻿using Coscine.SemanticSearch.Clients;
using Coscine.SemanticSearch.Rules;
using System;
using System.Collections.Generic;

namespace Coscine.SemanticSearch.Util;

/// <summary>
/// Contains methods for an application profile.
/// </summary>
public abstract class ApplicationProfile
{
    protected readonly RdfClient _rdfClient;
    protected readonly Lazy<IDictionary<string, LiteralRule>> _generalLiteralRules;

    /// <summary>
    /// Applicarion profile class constructor
    /// </summary>
    /// <param name="RDFClient">RDF Client</param>
    protected ApplicationProfile(RdfClient RDFClient)
    {
        _rdfClient = RDFClient;
        _generalLiteralRules = new(() => _rdfClient.ConstructLiteralRules(Uris.COSCINE_LITERAL_RULES));
    }
}