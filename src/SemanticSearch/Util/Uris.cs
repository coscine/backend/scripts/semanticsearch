namespace Coscine.SemanticSearch.Util
{
    /// <summary>
    /// Contains only constants for URIs and CURIEs.
    /// </summary>
    public static class Uris
    {
        // namespaces
        public const string RDFS = "http://www.w3.org/2000/01/rdf-schema#";
        public const string DCAT = "http://www.w3.org/ns/dcat#";
        public const string DCTERMS = "http://purl.org/dc/terms/";
        public const string SHACL = "http://www.w3.org/ns/shacl#";
        public const string RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
        public const string ORG = "http://www.w3.org/ns/org#";
        public const string OWL = "http://www.w3.org/2002/07/owl#";
        public const string QUDT = "http://qudt.org/schema/qudt/";
        public const string XSD = "http://www.w3.org/2001/XMLSchema#";
        public const string FOAF = "http://xmlns.com/foaf/0.1/";

        public const string COSCINE_PROJECTSTRUCTURE = "https://purl.org/coscine/terms/projectstructure#";
        public const string COSCINE_SEARCH = "https://purl.org/coscine/terms/search#";

        // SHACL
        public const string SH_NODE_SHAPE = "sh:NodeShape";
        public const string SH_PATH = "sh:path";
        public const string SH_PROPERTY = "sh:property";
        public const string SH_CLASS = "sh:class";
        public const string SH_NAME = "sh:name";
        public const string SH_DATATYPE = "sh:datatype";
        public const string SH_RULE = "sh:rule";
        public const string SH_PREFIX = "sh:prefix";
        public const string SH_PREFIXES = "sh:prefixes";
        public const string SH_SPARQL_RULE = "sh:SPARQLRule";
        public const string SH_SELECT = "sh:select";
        public const string SH_TARGET = "sh:target";
        public const string SH_CONSTRUCT = "sh:construct";
        public const string SH_DECLARE = "sh:declare";
        public const string SH_NAMESPACE = "sh:namespace";
        public const string SH_ORDER = "sh:order";

        // DCAT
        public const string DCTERMS_CONFORMS_TO = "dcterms:conformsTo";
        public const string DCTERMS_TITLE = "dcterms:title";

        // DCTERMS
        public const string DCAT_CATALOG = "dcat:catalog";

        // RDF
        public const string RDF_TYPE_LONG = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

        // FOAF
        public const string FOAF_PERSON = "foaf:Person";
        public const string FOAF_NAME = "foaf:name";
        public const string FOAF_GIVEN_NAME = "foaf:givenName";
        public const string FOAF_FAMILY_NAME = "foaf:familyName";

        // ORG
        public const string ORG_MEMBERSHIP = "org:Membership";
        public const string ORG_ORGANIZATION = "org:organization";
        public const string ORG_MEMBER = "org:member";
        public const string ORG_HAS_UNIT = "org:hasUnit";
        public const string ORG_ORGANIZATIONAL_COLLABORATION = "org:OrganizationalCollaboration";

        // RDFS
        public const string RDFS_SUBCLASS_OF = "rdfs:subClassOf";
        public const string RDFS_LABEL = "rdfs:label";

        // OWL
        public const string OWL_ONTOLOGY = "owl:Ontology";

        // FDP
        public const string FdpHasMetadata = "http://purl.org/fdp/fdp-o#hasMetadata";

        // PROV
        public const string ProvWasInvalidatedBy = "http://www.w3.org/ns/prov#wasInvalidatedBy";
        public const string ProvWasRevisionOf = "http://www.w3.org/ns/prov#wasRevisionOf";

        // QUDT
        public const string QUDT_VALUE = "qudt:value";
        public const string QUDT_UNIT = "qudt:unit";

        // COSCINE
        public const string COSCINE_LITERAL_RULES = "https://purl.org/coscine/rules/literal/";

        public const string COSCINE_PROJECTSTRUCTURE_IS_PUBLIC_LONG = "https://purl.org/coscine/terms/projectstructure#isPublic";
        public const string COSCINE_PROJECTSTRUCTURE_IS_PUBLIC = "coscineprojectstructure:isPublic";
        public const string COSCINE_PROJECTSTRUCTURE_IS_MEMBER_OF = "coscineprojectstructure:isMemberOf";
        public const string COSCINE_PROJECTSTRUCTURE_IS_FILE_OF = "coscineprojectstructure:isFileOf";
        public const string COSCINE_PROJECTSTRUCTURE_IS_RESOURCE_OF = "coscineprojectstructure:isResourceOf";

        public const string COSCINE_PROJECT_DELETED = "https://purl.org/coscine/terms/project#deleted";
        public const string COSCINE_RESOURCE_DELETED = "https://purl.org/coscine/terms/resource#deleted";

        public const string COSCINE_SEARCH_ROOT_CLASS = "https://purl.org/coscine/terms/search#rootClass";
        public const string COSCINE_SEARCH_ABSOLUTEFILENAME = "https://purl.org/coscine/terms/search#absoluteFileName";
        public const string COSCINE_SEARCH_FILENAME = "https://purl.org/coscine/terms/search#fileName";
        public const string COSCINE_SEARCH_GRAPHNAME = "https://purl.org/coscine/terms/search#graphName";
        public const string COSCINE_SEARCH_HOMEPAGE = "https://purl.org/coscine/terms/search#homepage";
        public const string COSCINE_SEARCH_VERSION = "https://purl.org/coscine/terms/search#version";
        public const string COSCINE_SEARCH_BELONGS_TO_PROJECT = "https://purl.org/coscine/terms/search#belongsToProject";
        public const string COSCINE_SEARCH_STRUCTURE_TYPE = "https://purl.org/coscine/terms/search#structureType";
        public const string COSCINE_SEARCH_APPLICATION_PROFILE = "https://purl.org/coscine/terms/search#applicationProfile";
        public const string COSCINE_SEARCH_CURRENT_INDEX = "https://purl.org/coscine/terms/search#currentIndex";
        public const string COSCINE_SEARCH_HAS_INDEX_VERSION = "https://purl.org/coscine/terms/search#hasIndexVersion";
        public const string COSCINE_SEARCH_IS_DELETED = "coscinesearch:isDeleted";

        // ruleset graphs
        // foaf
        public const string DFG = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/";
    }
}
