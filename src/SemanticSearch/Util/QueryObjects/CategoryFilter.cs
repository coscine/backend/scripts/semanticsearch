﻿using System.ComponentModel;
using System.Reflection;

namespace Coscine.SemanticSearch.Util.QueryObjects
{
    public enum CategoryFilter
    {
        [Description("None")]
        None,

        [Description("https://purl.org/coscine/terms/structure#Resource")]
        Resource,

        [Description("https://purl.org/coscine/terms/structure#Project")]
        Project,

        [Description("https://purl.org/coscine/terms/structure#Metadata")]
        Metadata
    }

    public static class CategoryFilterExtensions
    {
        public static string GetDescription(this CategoryFilter value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo == null) return null;
            var attribute = (DescriptionAttribute)fieldInfo.GetCustomAttribute(typeof(DescriptionAttribute));
            return attribute.Description;
        }
    }
}