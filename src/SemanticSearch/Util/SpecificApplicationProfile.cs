﻿using Coscine.SemanticSearch.Clients;
using Coscine.SemanticSearch.Rules;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.SemanticSearch.Util;

/// <summary>
/// Represents a specific application profile.
/// </summary>
public class SpecificApplicationProfile : ApplicationProfile
{
    public string ApplicationProfileId { get; set; }

    private readonly Lazy<IDictionary<string, LiteralRule>> _specificLiteralRules;

    private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

    /// <summary>
    /// Specific applicarion profile constructor
    /// </summary>
    /// <param name="rdfClient">RDF Client</param>
    /// <param name="applicationProfileId">Application Profile ID</param>
    public SpecificApplicationProfile(RdfClient rdfClient, string applicationProfileId) : base(rdfClient)
    {
        logger.Info("Initializing {applicationProfileId}", applicationProfileId);
        // Console.WriteLine($"Initializing {applicationProfileId}");
        ApplicationProfileId = applicationProfileId;

        _specificLiteralRules = new (() => _rdfClient.ConstructLiteralRules(ApplicationProfileId));
    }

    /// <summary>
    /// Creates JSON object for a given label (field) and instance (value)
    /// by applying the corresponding literal rules.
    /// </summary>
    /// <param name="label">The label of the field.</param>
    /// <param name="instance">An instance of a specific class for which a literal rule needs to be applied.</param>
    /// <param name="indexMapper">Instance that can retrieve property labels.</param>
    /// <returns>A JSON object containing the created field-value pair.</returns>
    public Document GetLiterals(string label, string instance, ElasticsearchIndexMapper indexMapper)
    {
        var literals = new List<string>();
        if (instance.Contains('@') && instance.Contains("&version="))
        {
            var literal = instance[(instance.LastIndexOf("/") + 1)..];
            literal = literal[..literal.IndexOf("@")];
            literal += string.Concat("@version=", instance.AsSpan(instance.IndexOf("&version=") + "&version=".Length));
            if (literal.Contains('&'))
            {
                literal = literal[..literal.IndexOf("&")];
            }
            literals.Add(literal);
        }
        if (literals.Count == 0)
        {
            var propertyLabel = indexMapper.GetLabelOfProperty(instance);
            if (propertyLabel is not null)
            {
                literals.Add(propertyLabel);
            }
        }
        if (literals.Count == 0)
        {
            // TODO: what about equivalent classes?
            var classes = _rdfClient.GetDirectClasses(instance);
            var constructRules = GetAllConstructRulesForLiterals(classes);
            foreach (var constructRule in constructRules)
            {
                literals.AddRange(constructRule.ExecuteRule(instance));
            }
            // if no literal could be found
            if (literals.Count == 0)
            {
                var literal = _rdfClient.GuessLabel(instance);
                if (string.IsNullOrEmpty(literal))
                {
                    throw new NotIndexableException("No literal could be generated for object.");
                }
                else
                {
                    literals.Add(literal);
                }
            }
        }

        return CreateDocumentFromList(label, literals.Distinct());
    }

    /// <summary>
    /// Creates a JToken of a list of literals.
    /// </summary>
    /// <param name="label">Current label.</param>
    /// <param name="list">A list of literals.</param>
    /// <returns>A JToken which is either a single string or an array.</returns>
    private static Document CreateDocumentFromList(string label, IEnumerable<string> list)
    {
        if (list.Count() == 1)
        {
            return new Document(label, list.First());
        }
        else
        {
            return new Document(label, list);
        }
    }

    /// <summary>
    /// Collects all relevant literal rules for the given classes.
    /// </summary>
    /// <remarks>Rules of direct classes are used. Rules for parent classes are only considered if
    /// no other rules could be found. A specific literal rule overwrites a generl literal rule for the same class.</remarks>
    /// <param name="classes">A list of classes for which literal rules are searched.</param>
    /// <returns>An enumerator of corresponding literal rules.</returns>
    private IEnumerable<LiteralRule> GetAllConstructRulesForLiterals(IEnumerable<string> classes)
    {
        // only use rules of all direct classes, if there are no: use rules of parents until root class is reached
        IList<LiteralRule> rules = new List<LiteralRule>();
        foreach (var classElement in classes)
        {
            // check for specific rule in application profile
            if (_specificLiteralRules.Value.ContainsKey(classElement))
            {
                rules.Add(_specificLiteralRules.Value[classElement]);
            }
            else
            {
                // only use general rule for this class if no specific rule for this class is defined in applicationprofile
                if (_generalLiteralRules.Value.ContainsKey(classElement))
                {
                    rules.Add(_generalLiteralRules.Value[classElement]);
                }
            }
        }
        if (rules.Count > 0)
        {
            return rules;
        }
        else
        {
            // if no rules could be found, search for rules of parent classes
            var parentClasses = _rdfClient.GetParentClasses(classes);
            if (parentClasses.Count == 0)
            {
                // use root class
                parentClasses.Add(Uris.COSCINE_SEARCH_ROOT_CLASS);
            }
            return GetAllConstructRulesForLiterals(parentClasses);
        }
    }
}