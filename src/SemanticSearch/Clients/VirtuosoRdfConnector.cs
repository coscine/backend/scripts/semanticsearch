﻿using Coscine.SemanticSearch.Core;
using Coscine.SemanticSearch.Util;
using System;
using System.Collections.Generic;
using VDS.RDF;
using VDS.RDF.Query;
using VDS.RDF.Storage;
using VDS.RDF.Update;

namespace Coscine.SemanticSearch.Clients
{
    /// <summary>
    /// Implements necessary functions for querying and manipulating the RDF-based knowledge graph.
    /// </summary>
    /// <inheritdoc cref="IRdfConnector"/>
    public class VirtuosoRdfConnector : IRdfConnector
    {
        private readonly SparqlRemoteUpdateEndpoint UpdateEndpoint;
        private readonly SparqlRemoteEndpoint QueryEndpoint;
        private readonly ReadWriteSparqlConnector ReadWriteSparqlConnector;

        private const string RULESET_QUERY = "DEFINE input:inference 'ruleset' ";

        // necessary namespaces
        private readonly IDictionary<string, Uri> NAMESPACES = new Dictionary<string, Uri>()
        {
            { "dcat", new Uri(Uris.DCAT) },
            { "dcterms", new Uri(Uris.DCTERMS) },
            { "rdfs", new Uri(Uris.RDFS) },
            { "rdf", new Uri(Uris.RDF) },
            { "sh", new Uri(Uris.SHACL) },
            { "foaf", new Uri(Uris.FOAF) },
            { "coscineprojectstructure", new Uri(Uris.COSCINE_PROJECTSTRUCTURE) },
            { "org", new Uri(Uris.ORG) },
            { "owl", new Uri(Uris.OWL) },
            { "qudt", new Uri(Uris.QUDT) },
            { "xsd", new Uri(Uris.XSD) },
            { "coscinesearch", new Uri(Uris.COSCINE_SEARCH) }
        };

        public VirtuosoRdfConnector(string sparqlEndpoint = "http://localhost:8890/sparql")
        {
            UpdateEndpoint = new SparqlRemoteUpdateEndpoint(new Uri(string.Format(sparqlEndpoint)));
            QueryEndpoint = new SparqlRemoteEndpoint(new Uri(string.Format(sparqlEndpoint)));
            ReadWriteSparqlConnector = new ReadWriteSparqlConnector(QueryEndpoint, UpdateEndpoint);

            Options.InternUris = false;
            Options.FullTripleIndexing = false;

            // 100 second timeout
            const int timeout = 100000;
            QueryEndpoint.Timeout = timeout;
            UpdateEndpoint.Timeout = timeout;
            ReadWriteSparqlConnector.Timeout = timeout;
        }

        private SparqlParameterizedString NewSparqlParameterizedString()
        {
            var _queryString = new SparqlParameterizedString();
            foreach (var nameSpace in NAMESPACES)
            {
                _queryString.Namespaces.AddNamespace(nameSpace.Key, nameSpace.Value);
            }
            return _queryString;
        }

        public IGraph GetGraph(string graphName)
        {
            var graph = new Graph();
            ReadWriteSparqlConnector.LoadGraph(graph, graphName);
            return graph;
        }

        /// <summary>
        /// Builds the final SPARQLL query which contains possible inference rules and namespaces.
        /// </summary>
        /// <param name="query">String representation of a SPARQL query.</param>
        /// <param name="withInference">Flag which indicates if inference rules should be used.</param>
        /// <returns>Final SPARQL query string.</returns>
        private string GetFinalQueryString(string query, bool withInference = true)
        {
            var _queryString = NewSparqlParameterizedString();
            _queryString.CommandText = query;

            if (withInference)
            {
                return RULESET_QUERY + _queryString.ToString();
            }
            else
            {
                return _queryString.ToString();
            }
        }

        public IGraph QueryWithResultGraph(SparqlParameterizedString query)
        {
            return (IGraph)ReadWriteSparqlConnector.Query(GetFinalQueryString(query.ToString(), false));
        }

        public SparqlResultSet QueryWithResultSet(string query, bool withInference = true)
        {
            // In case of undefined rules, the first query will fail
            SparqlResultSet resultSet;
            try
            {
                resultSet = (SparqlResultSet)ReadWriteSparqlConnector.Query(GetFinalQueryString(query, withInference));
            }
            catch (Exception)
            {
                resultSet = (SparqlResultSet)ReadWriteSparqlConnector.Query(GetFinalQueryString(query, false));
            }
            return resultSet;
        }

        public SparqlResultSet QueryWithResultSet(SparqlParameterizedString query)
        {
            var _queryString = NewSparqlParameterizedString();
            _queryString.CommandText = query.ToString();
            return (SparqlResultSet)ReadWriteSparqlConnector.Query(_queryString.ToString());
        }

        public void Update(SparqlParameterizedString query)
        {
            var _queryString = NewSparqlParameterizedString();
            _queryString.CommandText = query.ToString();
            ReadWriteSparqlConnector.Update(_queryString.ToString());
        }
    }
}