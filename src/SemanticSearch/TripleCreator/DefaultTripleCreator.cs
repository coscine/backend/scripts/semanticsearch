﻿using Coscine.SemanticSearch.Util;
using VDS.RDF;

namespace Coscine.SemanticSearch.TripleCreator
{
    public class DefaultTripleCreator
    {
        public virtual Document CreateFields(IGraph graph, ElasticsearchIndexMapper indexMapper)
        {
            return new Document();
        }
    }
}
