﻿using Coscine.SemanticSearch.Util;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using VDS.RDF;

namespace Coscine.SemanticSearch.TripleCreator
{
    public class ProjectTripleCreator : DefaultTripleCreator
    {
        private static readonly Regex guidRegexPattern = new("([0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12})");
        private readonly IDictionary<string, string> _projectSlugs;

        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        public ProjectTripleCreator(IDictionary<string, string> projectSlugs)
        {
            _projectSlugs = projectSlugs;
        }

        public override Document CreateFields(IGraph graph, ElasticsearchIndexMapper indexMapper)
        {
            var coscineFields = new List<string>()
            {
                Uris.COSCINE_SEARCH_BELONGS_TO_PROJECT,
                Uris.COSCINE_SEARCH_GRAPHNAME,
                Uris.COSCINE_SEARCH_STRUCTURE_TYPE,
                Uris.COSCINE_PROJECTSTRUCTURE_IS_PUBLIC_LONG,
            };
            var result = new Document();

            var projectId = Guid.Parse(guidRegexPattern.Match(graph.BaseUri.ToString()).Value); // Handle null.Value case?
            var slugTriple = graph.GetTriplesWithPredicate(new Uri("https://purl.org/coscine/terms/project#slug")).FirstOrDefault();
            if (slugTriple?.Object is ILiteralNode && !_projectSlugs.ContainsKey(projectId.ToString()))
            {
                _projectSlugs.Add(projectId.ToString(), (slugTriple.Object as ILiteralNode).Value);
            }

            foreach (var property in coscineFields)
            {
                var label = indexMapper.GetLabelOfProperty(property);
                if (string.IsNullOrEmpty(label))
                {
                    logger.Info("Property {property} could not be indexed because no label was found.", property);
                    // Console.WriteLine($"Property {property} could not be indexed because no label was found.");
                    continue;
                }
                switch (property)
                {
                    case Uris.COSCINE_SEARCH_BELONGS_TO_PROJECT:
                        result.Add(label, $"https://purl.org/coscine/projects/{projectId}");
                        break;

                    case Uris.COSCINE_SEARCH_GRAPHNAME:
                        result.Add(label, graph.BaseUri.ToString());
                        break;

                    case Uris.COSCINE_SEARCH_STRUCTURE_TYPE:
                        result.Add(label, "https://purl.org/coscine/terms/structure#Project");
                        break;

                    case Uris.COSCINE_PROJECTSTRUCTURE_IS_PUBLIC_LONG:
                        result.Add(label, graph.GetTriplesWithPredicate(new Uri("https://purl.org/coscine/terms/project#visibility"))
                            .Any((triple) => triple.Object.ToString() == "https://purl.org/coscine/terms/visibility#public"));
                        break;

                    default:
                        continue;
                }
            }
            return result;
        }
    }
}