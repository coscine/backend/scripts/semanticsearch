﻿using NLog;
using NLog.LayoutRenderers;
using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Coscine.SemanticSearch.Logging
{
    [LayoutRenderer("assembly-name")]
    public class AssemblyNameLayoutRenderer : LayoutRenderer
    {
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var assembly = Assembly.GetExecutingAssembly().GetName();
            if (assembly is not null)
            {
                builder.Append(assembly.Name);
            }
            else
            {
                builder.Append(new Guid().ToString().Take(8));
            }
        }
    }
}
